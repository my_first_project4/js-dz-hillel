let someNum;
do { someNum = prompt('Enter number');}
    while (someNum <= 0 || someNum === null || someNum.trim() === ''|| isNaN(someNum))

someNum = +someNum;

let resultI;
let sumOdd = 0;
for (let i = 0; i <= someNum; i++) {
    // если true, пропустить оставшуюся часть тела цикла
    if (i % 2 === 0) continue;
    sumOdd += i;
    console.log(sumOdd); // 1, 3, 5, 7, 9
}

let resultJ;
let sumEven = 0;
for (let j = 2; j <= someNum; j++) {
    // если true, пропустить оставшуюся часть тела цикла
    if (j % 2 !== 0) continue;
    sumEven += j;
    console.log(sumEven); // 2, 3, 4, 6, 8, 10
}

function calcSumI () {
    resultI = sumOdd;
    return resultI;
}
alert(calcSumI());

function calcSumJ () {
    resultJ = sumEven;
    return resultJ;
}
alert(calcSumJ());














