


let answerOne = prompt('Enter first number');
while (answerOne === null || answerOne.trim() === ''|| isNaN(answerOne)) {
    answerOne = prompt('Try again');

}

let answerTwo = prompt('Enter second number');
while (answerTwo === null || answerTwo.trim() === '' || isNaN(answerTwo)) {
    answerTwo = prompt('Try again');
}

let answerThree = prompt('Choose an action: "+", "-", "/", "*" ');
while (answerThree !== '+' && answerThree !== '-' && answerThree !== '/' && answerThree !== '*') {
    answerThree = prompt('Try again');
}

switch (answerThree) {
    case '+': alert(answerOne + answerThree + answerTwo + '=' + (+answerOne + +answerTwo));
    break;
    case '-': alert(answerOne + answerThree + answerTwo + '=' + (+answerOne - +answerTwo));
    break;
    case '/': alert(answerOne + answerThree + answerTwo + '=' + (+answerOne / +answerTwo));
    break;
    case '*': alert(answerOne + answerThree + answerTwo + '=' + (+answerOne * +answerTwo));
    break;

}